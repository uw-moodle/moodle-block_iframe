<?php //$Id: block_html.php,v 1.8 2005/05/19 20:09:57 defacer Exp $

class block_iframe extends block_base {

    function init() {
        $this->title = get_string('pluginname', 'block_iframe');
    }

    function applicable_formats() {
        return array('all' => true);
    }

    function specialization() {
        $this->title = isset($this->config->title) ? format_string($this->config->title) : format_string(get_string('newiframeblock', 'block_iframe'));
    }

    function instance_allow_multiple() {
        return true;
    }

    function get_content() {
        global $CFG, $COURSE;
        if ($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass;
        if (!empty($this->config->url)) {
            $search = array('/\$idnumber/', '/\$id/');
            $replace = array("$COURSE->idnumber", "$COURSE->id");
            $url = preg_replace($search, $replace, $this->config->url);
            $this->content->text = '<iframe src="'.$url.'"'.
                (isset($this->config->width) ? ' width="'.$this->config->width.'"' : '').
                (isset($this->config->height) ? ' height="'.$this->config->height.'"' : '').
                (isset($this->config->style) ? ' style="'.$this->config->style.'"' : '').
                (isset($this->config->cssclass) ? ' class="'.$this->config->cssclass.'"' : '').
                (isset($this->config->scrolling) ? ' scrolling="'.$this->config->scrolling.'"' : '').
                (isset($this->config->name) ? ' name="'.$this->config->name.'"' : '').
                (isset($this->config->titleattr) ? ' title="'.$this->config->titleattr.'"' : '').
                (isset($this->config->iframeid) ? ' id="'.$this->config->iframeid.'"' : '').
                '></iframe>';
        }
        $this->content->footer = '';

        return $this->content;
    }
}
?>

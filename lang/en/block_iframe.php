<?PHP
$string['pluginname'] = 'iFrame Block';
$string['blockname'] = 'iFrame';
$string['iframe:addinstance'] = 'Add iFrame block';

$string['configtitle'] = 'Block Title';
$string['configurl'] = 'iframe src attribute (URL)';
$string['vardesc'] = 'You can use the following variables in the URL:
<ul>
<li>$id = the Moodle course id</li>
<li>$idnumber = the Course ID Number field value from the course setup page</li>
</ul>';
$string['configid'] = 'iframe id attribute';
$string['configcssclass'] = 'iframe class attribute';
$string['configheight'] = 'iframe height attribute';
$string['configwidth'] = 'iframe width attribute';
$string['configname'] = 'iframe name attribute';
$string['configscrolling'] = 'iframe scrolling attribute';
$string['configstyle'] = 'iframe style attribute';
$string['configtitleattr'] = 'iframe title attribute';
$string['newiframeblock'] = '(new iframe block)';


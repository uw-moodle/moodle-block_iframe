<?php
class block_iframe_edit_form extends block_edit_form {
    protected function specific_definition($mform) {
        $mform->addElement('header','configheader', get_string('blocksettings', 'block'));

        $mform->addElement('text', 'config_title', get_string('configtitle', 'block_iframe'),array('size'=>'30'));
        $mform->setType('config_title',PARAM_TEXT);

        $mform->addElement('text', 'config_url', get_string('configurl', 'block_iframe'),array('size'=>'60'));
        $mform->setType('config_url',PARAM_URL);
        $mform->addElement('static','vardesc','',get_string('vardesc','block_iframe'));

        $mform->addElement('text', 'config_height', get_string('configheight', 'block_iframe'),array('size'=>'3'));
        $mform->setDefault('config_height', '150');
        $mform->setType('config_height',PARAM_INT);
        $mform->addElement('text', 'config_width', get_string('configwidth', 'block_iframe'),array('size'=>'3'));
        $mform->setDefault('config_width', '150');
        $mform->setType('config_width',PARAM_INT);
        $mform->addElement('text', 'config_style', get_string('configstyle', 'block_iframe'),array('size'=>'60'));
        $mform->setType('config_style',PARAM_TEXT);
        $mform->addElement('text', 'config_cssclass', get_string('configcssclass', 'block_iframe'),array('size'=>'60'));
        $mform->setType('config_cssclass',PARAM_TEXT);
        $mform->addElement('text', 'config_id', get_string('configid', 'block_iframe'),array('size'=>'60'));
        $mform->setType('config_id',PARAM_TEXT);
        $mform->addElement('text', 'config_titleattr', get_string('configtitleattr', 'block_iframe'),array('size'=>'60'));
        $mform->setType('config_titleattr',PARAM_TEXT);
        $mform->addElement('text', 'config_name', get_string('configname', 'block_iframe'),array('size'=>'60'));
        $mform->setType('config_name',PARAM_TEXT);

        $scrolloptions = array('auto'=>'Automatic','yes'=>'Yes','no'=>'No');
        $mform->addElement('select', 'config_scrolling', get_string('configscrolling', 'block_iframe'),$scrolloptions);
        $mform->setDefault('config_scrolling', 'auto');
        $mform->setType('config_width',PARAM_INT);
    }
}